package fr.qilat.connect4.utils;

public class Logger {
    
    public static void info(String log) {
        System.out.println("[Connect 4] " + log);
    }
    
    public static void err(String log) {
        System.err.println("[Connect 4] " + log);
    }
    
}
