package fr.qilat.connect4.utils;

public interface IModule {
    
    void start();
    
    void update();
    
    void stop();
}
