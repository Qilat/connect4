package fr.qilat.connect4.server.console;

import fr.qilat.connect4.utils.IModule;
import fr.qilat.connect4.utils.Logger;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public class ConsoleModule implements IModule {
    
    @Getter
    private final CommandRegister register;
    private final LinkedBlockingQueue<Runnable> commandsExecutions;
    
    private final Thread thread;
    private final ConsoleRunnable consoleRunnable;
    
    public ConsoleModule() {
        this.register = new CommandRegister();
        this.commandsExecutions = new LinkedBlockingQueue<>();
        this.consoleRunnable = new ConsoleRunnable(this.register, this.commandsExecutions);
        this.thread = new Thread(this.consoleRunnable, "Thread-Console");
    }
    
    @Override
    public void start() {
        Logger.info("Starting console module.");
        this.thread.start();
    }
    
    @Override
    public void update() {
        List<Runnable> runnables = new ArrayList<>();
        commandsExecutions.drainTo(runnables);
        for (Runnable runnable : runnables) {
            runnable.run();
        }
    }
    
    @Override
    public void stop() {
        Logger.info("Stopping console module.");
        this.consoleRunnable.shouldStop();
    }
    
    public boolean isRunning() {
        return this.consoleRunnable.isRunning();
    }
}
