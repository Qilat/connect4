package fr.qilat.connect4.server.netty.packets;

import io.netty.buffer.ByteBuf;
import lombok.ToString;

@ToString(callSuper = true)
public class PingPacket extends Packet {
    
    public PingPacket(Integer id) {
        super(id);
    }
    
    public PingPacket() {
        super(0x01);
    }
    
    @Override
    public void decode(ByteBuf in) {
    }
    
    @Override
    public void encode(ByteBuf out) {
        out.writeInt(this.getId());
    }
    
    
}
