package fr.qilat.connect4.server.netty.packets;

import io.netty.buffer.ByteBuf;
import io.netty.util.CharsetUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@ToString(callSuper = true)
public class UuidPacket extends Packet {
    
    @Getter
    @Setter
    private UUID uuid;
    
    public UuidPacket(Integer id) {
        super(id);
    }
    
    public UuidPacket() {
        super(0x02);
    }
    
    public UuidPacket(UUID uuid) {
        this();
        this.uuid = uuid;
    }
    
    @Override
    public void decode(ByteBuf in) {
        uuid = UUID.fromString(in.readCharSequence(36, CharsetUtil.UTF_8).toString());
    }
    
    @Override
    public void encode(ByteBuf out) {
        out.writeInt(this.getId());
        out.writeCharSequence(this.getUuid().toString(), CharsetUtil.UTF_8);
    }
    
}
