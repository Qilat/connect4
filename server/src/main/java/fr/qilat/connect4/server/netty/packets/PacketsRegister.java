package fr.qilat.connect4.server.netty.packets;

import java.util.HashMap;

public class PacketsRegister {
    
    private static final HashMap<Integer, Class<? extends Packet>> packetsList = new HashMap<Integer, Class<? extends Packet>>();
    
    static {
        packetsList.put(0x01, PingPacket.class);
        packetsList.put(0x02, UuidPacket.class);
    }
    
    public static Class<? extends Packet> get(int id) {
        return packetsList.get(id);
    }
    
}
