package fr.qilat.connect4.server.netty.packets;

import fr.qilat.connect4.server.netty.ChannelsHandler;
import fr.qilat.connect4.server.netty.listeners.ListenersHandler;
import fr.qilat.connect4.utils.Logger;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.util.UUID;

/**
 * Handles a server-side channel.
 */
public class PacketServerHandler extends ChannelInboundHandlerAdapter { // (1)
    
    private final ChannelsHandler channelsHandler;
    private final ListenersHandler listenersHandler;
    
    public PacketServerHandler(ChannelsHandler channelsHandler, ListenersHandler listenersHandler) {
        this.channelsHandler = channelsHandler;
        this.listenersHandler = listenersHandler;
    }
    
    @Override
    public void handlerAdded(ChannelHandlerContext ctx) {
        UUID uuid = this.channelsHandler.register(ctx.channel());
        Logger.info(uuid + " is connected.");
    }
    
    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) {
        Logger.info("Handler removed");
        this.channelsHandler.unregister(ctx);
    }
    
    @Override
    public void channelActive(ChannelHandlerContext ctx) {
    }
    
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        this.listenersHandler.addPacket((Packet) msg);
    }
    
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        Logger.err(cause.getMessage());
        cause.printStackTrace();
        ctx.close();
        this.channelsHandler.unregister(ctx);
    }
}
