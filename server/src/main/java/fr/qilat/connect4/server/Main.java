package fr.qilat.connect4.server;

public class Main {
    public static void main(String[] args) throws Exception {
        int port = 8080;
        if (args.length > 0) {
            port = Integer.parseInt(args[0]);
        }
    
        Connect4Server connect4Server = new Connect4Server(port);
        connect4Server.start();
    }
}
