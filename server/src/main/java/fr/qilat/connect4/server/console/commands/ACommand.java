package fr.qilat.connect4.server.console.commands;

public abstract class ACommand {
    public abstract Runnable execute(String[] args);
}
