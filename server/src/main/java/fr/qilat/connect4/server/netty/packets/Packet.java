package fr.qilat.connect4.server.netty.packets;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public abstract class Packet {
    
    @Getter
    @Setter
    private int id;
    
    @Getter
    @Setter
    private Channel channel;
    
    public Packet(int id) {
        this.id = id;
    }
    
    public abstract void decode(ByteBuf in);
    
    public abstract void encode(ByteBuf out);
    
}
