package fr.qilat.connect4.server.netty.packets;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

public class PacketDecoder extends ByteToMessageDecoder { // (1)
    
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) { // (2)
        int id = in.readInt();
        try {
            Packet packet = PacketsRegister.get(id).getConstructor(Integer.class).newInstance(id);
            packet.decode(in);
            packet.setChannel(ctx.channel());
            out.add(packet);
        } catch (Exception e) {
            e.printStackTrace();
            out.add(null);
        }
    }
}
