package fr.qilat.connect4.server.netty.listeners;

import fr.qilat.connect4.server.netty.packets.Packet;
import fr.qilat.connect4.utils.Logger;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

public class ListenersHandler {
    
    private final LinkedBlockingQueue<Packet> packets;
    private final Map<Class<? extends Packet>, List<ListenerMethod>> listeners;
    
    public ListenersHandler() {
        this.packets = new LinkedBlockingQueue<>();
        this.listeners = new HashMap<>();
    }
    
    public void registerListener(PacketListenersContainer plc) {
        for (Method method : plc.getClass().getDeclaredMethods()) {
            for (Annotation annotation : method.getDeclaredAnnotations()) {
                if (PacketListener.class.isAssignableFrom(annotation.getClass())) {
                    PacketListener castedAntn = (PacketListener) annotation;
                    List<ListenerMethod> list = listeners.computeIfAbsent(castedAntn.value(), k -> new ArrayList<>());
                    list.add(new ListenerMethod(method, plc));
                }
            }
        }
    }
    
    public void processReceivedPackets() {
        List<Packet> processedPackets = new ArrayList<>();
        packets.drainTo(processedPackets);
        for (Packet processedPacket : processedPackets) {
            Logger.info(processedPacket.toString());
            List<ListenerMethod> listenerMethods = listeners.get(processedPacket.getClass());
            for (ListenerMethod listenerMethod : listenerMethods) {
                try {
                    listenerMethod.getMethod().invoke(listenerMethod.getObject(), processedPacket);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
    
    public void addPacket(Packet packet) {
        try {
            this.packets.put(packet);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    
    @Getter
    @AllArgsConstructor
    private class ListenerMethod {
        private Method method;
        private Object object;
    }
    
}
