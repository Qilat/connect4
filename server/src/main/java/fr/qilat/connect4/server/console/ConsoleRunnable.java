package fr.qilat.connect4.server.console;

import fr.qilat.connect4.server.console.commands.ACommand;
import fr.qilat.connect4.utils.Logger;

import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.LinkedBlockingQueue;

public class ConsoleRunnable implements Runnable {
    
    private final LinkedBlockingQueue<Runnable> commandsExecutions;
    private final CommandRegister register;
    private boolean shouldContinue = true;
    
    public ConsoleRunnable(CommandRegister register, LinkedBlockingQueue<Runnable> commandsExecutions) {
        this.register = register;
        this.commandsExecutions = commandsExecutions;
    }
    
    @Override
    public void run() {
        Scanner sc = new Scanner(System.in);
        do {
            String[] input = sc.nextLine().split(" ");
            ACommand command = this.register.getCommand(input[0]);
            if (command == null) {
                Logger.err("Commande inconnue : " + input[0]);
            } else {
                Runnable runnable = command.execute(Arrays.copyOfRange(input, 1, input.length));
                if (runnable != null) {
                    try {
                        this.commandsExecutions.put(runnable);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        } while (shouldContinue);
    }
    
    public void shouldStop() {
        this.shouldContinue = false;
    }
    
    public boolean isRunning() {
        return this.shouldContinue;
    }
    
}
