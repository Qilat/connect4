package fr.qilat.connect4.server.netty;

import fr.qilat.connect4.server.netty.packets.Packet;
import fr.qilat.connect4.utils.Logger;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;

import java.util.UUID;

public class ChannelsHandler {
    
    private final BidiMap<UUID, Channel> channels;
    
    public ChannelsHandler() {
        this.channels = new DualHashBidiMap<>();
    }
    
    public boolean isUuidAlreadyRegistered(UUID uuid) {
        return this.channels.containsKey(uuid);
    }
    
    public UUID register(Channel channel) {
        if (this.channels.containsValue(channel)) {
            UUID uuid = this.channels.getKey(channel);
            System.err.println("ChannelHandlerContext already registered under UUID " + uuid);
            return uuid;
        }
        
        UUID uuid;
        while (this.isUuidAlreadyRegistered(uuid = UUID.randomUUID())) {
        }
        this.channels.put(uuid, channel);
        return uuid;
    }
    
    public boolean unregister(UUID uuid) {
        return this.channels.remove(uuid) != null;
    }
    
    public boolean unregister(ChannelHandlerContext channelHandlerContext) {
        return this.channels.removeValue(channelHandlerContext) != null;
    }
    
    public UUID getUUID(Channel channel) {
        return this.channels.getKey(channel);
    }
    
    public void sendPacket(UUID uuid, Packet packet) {
        ChannelFuture future = this.channels.get(uuid).writeAndFlush(packet);
        future.addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) {
                Logger.info(packet.getClass().getCanonicalName() + " sent to " + uuid.toString());
            }
        });
        
    }
}
