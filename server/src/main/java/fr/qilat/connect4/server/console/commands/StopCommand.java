package fr.qilat.connect4.server.console.commands;

import fr.qilat.connect4.server.console.ConsoleModule;
import fr.qilat.connect4.utils.IModule;

public class StopCommand extends ACommand {
    
    private final IModule mainModule;
    private final ConsoleModule consoleModule;
    
    public StopCommand(IModule module, ConsoleModule consoleModule) {
        this.mainModule = module;
        this.consoleModule = consoleModule;
    }
    
    @Override
    public Runnable execute(String[] args) {
        this.consoleModule.stop();
        return this.mainModule::stop;
    }
}
