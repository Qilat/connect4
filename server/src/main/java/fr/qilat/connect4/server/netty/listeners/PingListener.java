package fr.qilat.connect4.server.netty.listeners;

import fr.qilat.connect4.server.netty.ChannelsHandler;
import fr.qilat.connect4.server.netty.packets.PingPacket;
import fr.qilat.connect4.server.netty.packets.UuidPacket;

import java.util.UUID;

public class PingListener implements PacketListenersContainer {
    private final ChannelsHandler channelsHandler;
    public PingListener(ChannelsHandler channelsHandler) {
        this.channelsHandler = channelsHandler;
    }
    
    @PacketListener(PingPacket.class)
    public void attribUUID(PingPacket packet) {
        UUID uuid = this.channelsHandler.getUUID(packet.getChannel());
        UuidPacket response = new UuidPacket(uuid);
        this.channelsHandler.sendPacket(uuid, response);
    }
}
