package fr.qilat.connect4.server.console;

import fr.qilat.connect4.server.console.commands.ACommand;

import java.util.HashMap;
import java.util.Map;

public class CommandRegister {
    
    private Map<String, ACommand> commandMap;
    
    public CommandRegister() {
        this.commandMap = new HashMap<>();
    }
    
    public ACommand getCommand(String name) {
        return commandMap.get(name);
    }
    
    public void registerCommand(String name, ACommand command) {
        this.commandMap.put(name, command);
    }
    
    public void unregisterCommand(String name) {
        this.commandMap.remove(name);
    }
    
}
