package fr.qilat.connect4.server.netty;

import fr.qilat.connect4.server.netty.listeners.ListenersHandler;
import fr.qilat.connect4.server.netty.packets.PacketDecoder;
import fr.qilat.connect4.server.netty.packets.PacketEncoder;
import fr.qilat.connect4.server.netty.packets.PacketServerHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;

public class NettyServer implements Runnable {
    
    private final int port;
    private final EventLoopGroup bossGroup;
    private final EventLoopGroup workerGroup;
    private ChannelFuture channelFuture;
    private final ChannelsHandler channelsHandler;
    private final ListenersHandler listenersHandler;
    
    public NettyServer(int port, ChannelsHandler channelsHandler, ListenersHandler listenersHandler) {
        this.port = port;
        this.bossGroup = new NioEventLoopGroup();
        this.workerGroup = new NioEventLoopGroup();
        this.channelsHandler = channelsHandler;
        this.listenersHandler = listenersHandler;
    }
    
    public void run() {
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() { // (4)
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline().addLast(
                                    new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4),
                                    new LengthFieldPrepender(4),
                                    new PacketDecoder(),
                                    new PacketEncoder(),
                                    new PacketServerHandler(NettyServer.this.channelsHandler, NettyServer.this.listenersHandler));
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);
            
            this.channelFuture = b.bind(port).sync();
            
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    
    public void stop() {
        this.channelFuture.channel().close();
        this.workerGroup.shutdownGracefully();
        this.bossGroup.shutdownGracefully();
    }
    
}
