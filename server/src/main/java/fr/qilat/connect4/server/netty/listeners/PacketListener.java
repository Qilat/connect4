package fr.qilat.connect4.server.netty.listeners;

import fr.qilat.connect4.server.netty.packets.Packet;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface PacketListener {
    Class<? extends Packet> value();
}
