package fr.qilat.connect4.server.netty;

import fr.qilat.connect4.server.netty.listeners.ListenersHandler;
import fr.qilat.connect4.server.netty.listeners.PingListener;
import fr.qilat.connect4.utils.IModule;
import fr.qilat.connect4.utils.Logger;
import lombok.Getter;

public class NettyServerModule implements IModule {
    
    private final NettyServer nettyServer;
    @Getter
    private final ListenersHandler listenersHandler;
    @Getter
    private final ChannelsHandler channelsHandler;
    
    public NettyServerModule(int port) {
        this.channelsHandler = new ChannelsHandler();
        this.listenersHandler = new ListenersHandler();
        this.nettyServer = new NettyServer(port, this.channelsHandler, this.listenersHandler);
    }
    
    @Override
    public void start() {
        Logger.info("Starting Netty module.");
        this.listenersHandler.registerListener(new PingListener(this.channelsHandler));
        this.nettyServer.run();
    }
    
    @Override
    public void update() {
        this.listenersHandler.processReceivedPackets();
    }
    
    @Override
    public void stop() {
        Logger.info("Stopping Netty module.");
        this.nettyServer.stop();
    }
}
