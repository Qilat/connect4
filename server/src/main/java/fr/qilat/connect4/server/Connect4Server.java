package fr.qilat.connect4.server;

import fr.qilat.connect4.server.console.ConsoleModule;
import fr.qilat.connect4.server.console.commands.StopCommand;
import fr.qilat.connect4.server.netty.NettyServerModule;
import fr.qilat.connect4.utils.IModule;
import fr.qilat.connect4.utils.Logger;
import lombok.Getter;

public class Connect4Server implements IModule {
    
    private static Connect4Server CONNECT_4_SERVER;
    private final NettyServerModule nettyModule;
    
    Connect4Server(int port) {
        CONNECT_4_SERVER = this;
        this.nettyModule = new NettyServerModule(port);
        this.consoleModule = new ConsoleModule();
    }
    
    @Getter
    private final ConsoleModule consoleModule;
    private boolean shouldStop = false;
    
    public static Connect4Server get() {
        return CONNECT_4_SERVER;
    }
    
    @Override
    public void start() {
        Logger.info("Starting all modules.");
        this.nettyModule.start();
        
        this.consoleModule.getRegister().registerCommand("stop", new StopCommand(this, this.consoleModule));
        this.consoleModule.start();
        
        while (!shouldStop) {
            this.consoleModule.update();
            this.nettyModule.update();
        }
    }
    
    @Override
    public void update() {
    
    }
    
    @Override
    public void stop() {
        Logger.info("Stopping all modules.");
        
        this.shouldStop = true;
        this.nettyModule.stop();
        
        if (this.consoleModule.isRunning())
            System.exit(0);
    }
}
