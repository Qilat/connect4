package fr.qilat.connect4.client;

public class Main {
    
    public static void main(String[] args) throws Exception {
        String address = "localhost";
        int port = 8080;
    
        Connect4Client client = new Connect4Client(address, port);
        client.start();
    }
    
}
