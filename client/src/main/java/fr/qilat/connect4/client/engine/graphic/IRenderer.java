package fr.qilat.connect4.client.engine.graphic;

import fr.qilat.connect4.client.engine.IGui;
import fr.qilat.connect4.client.engine.Scene;
import fr.qilat.connect4.client.engine.Window;


/**
 * Created by Qilat on 2019-03-29 for goldwyn.
 */
public interface IRenderer {
    
    void init(Window window) throws Exception;
    
    void render(Window window, Camera camera, Scene scene, IGui hud);
    
    void cleanup();
    
}
