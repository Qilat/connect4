package fr.qilat.connect4.client.netty;

import fr.qilat.connect4.server.netty.listeners.ListenersHandler;
import fr.qilat.connect4.server.netty.packets.Packet;
import fr.qilat.connect4.server.netty.packets.PingPacket;
import fr.qilat.connect4.utils.Logger;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.Getter;

public class PacketClientHandler extends ChannelInboundHandlerAdapter {
    @Getter
    private ChannelHandlerContext ctx;
    @Getter
    private final ListenersHandler listenersHandler;
    private final NettyClient nettyClient;
    
    public PacketClientHandler(NettyClient client) {
        this.listenersHandler = client.getListenersHandler();
        this.nettyClient = client;
    }
    
    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        this.nettyClient.setConnected(true);
        this.ctx = ctx;
        this.ctx.channel().writeAndFlush(new PingPacket());
    }
    
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        this.listenersHandler.addPacket((Packet) msg);
    }
    
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }
    
    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) {
        if (this.nettyClient.isConnected()) {
            Logger.info("Netty server connection lost.");
            this.nettyClient.setConnected(false);
        }
        this.nettyClient.initiateRestartThread();
    }
    
    
}
