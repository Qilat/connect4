package fr.qilat.connect4.client.engine;

import fr.qilat.connect4.client.engine.item.GameItem;
import lombok.Getter;

import java.util.ArrayList;

/**
 * Created by Theophile on 2018-12-12 for projecty.
 */
public class Scene {
    
    @Getter
    private final ArrayList<GameItem> gameItems;
    
    public Scene() {
        gameItems = new ArrayList<>();
    }
    
    public void init() throws Exception {
        for (GameItem gameItem : this.gameItems) {
            gameItem.init();
        }
    }
    
    public void input() {
        for (GameItem gameItem : this.gameItems) {
            gameItem.input();
        }
    }
    
    public void update(float interval) {
        for (GameItem gameItem : this.gameItems) {
            gameItem.update(interval);
        }
    }
    
    public void cleanup() {
        for (GameItem item : this.gameItems) {
            if (item.isVisible())
                item.getMesh().cleanUp();
        }
    }
    
    public void addGameItem(GameItem gameItem) {
        this.gameItems.add(gameItem);
    }
}
