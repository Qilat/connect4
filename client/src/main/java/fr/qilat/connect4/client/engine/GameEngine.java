package fr.qilat.connect4.client.engine;

import fr.qilat.connect4.client.engine.input.Input;
import lombok.Getter;
import org.lwjgl.glfw.GLFW;

/**
 * Created by Qilat on 2019-03-27 for goldwyn.
 */
public class GameEngine implements Runnable {
    
    private static final int TARGET_FPS = 60;
    private static final int TARGET_UPS = 120;
    public static int FPS = 0;
    
    @Getter
    private final Window window;
    private final Thread gameLoopThread;
    private final Timer timer;
    private final IGameLogic stage;
    
    public GameEngine(String windowTitle, int width, int height, boolean vSync, boolean resizable, IGameLogic stage) {
        this.window = new Window(windowTitle, width, height, vSync, resizable);
        this.gameLoopThread = new Thread(this, "GAME_LOOP_THREAD");
        this.stage = stage;
        this.timer = new Timer();
    }
    
    public void start() {
        String osName = System.getProperty("os.name");
        if (osName.contains("Mac")) {
            gameLoopThread.run();
        } else {
            gameLoopThread.start();
        }
    }
    
    @Override
    public void run() {
        try {
            init();
            loop();
        } catch (Exception e) {
            if (Options.DEBUG_OUTPUT)
                e.printStackTrace();
        } finally {
            cleanup();
        }
    }
    
    private void init() throws Exception {
        window.init();
        timer.init();
        
        stage.init(window);
    }
    
    private void loop() throws Exception {
        float elapsedTime;
        float accumulator = 0f;
        float interval = 1f / TARGET_UPS;
        
        
        while (!window.isClosed()) {
            elapsedTime = timer.getElapsedTime();
            accumulator += elapsedTime;
            
            input();
            
            while (accumulator >= interval) {
                update(interval);
                accumulator -= interval;
            }
            FPS = (int) (1 / elapsedTime);
            render();
            
            
            if (!window.isVsync()) {
                sync();
            }
        }
    }
    
    private void input() throws Exception {
        stage.input(window);
    }
    
    private void update(float interval) throws Exception {
        if (Input.keyDown(GLFW.GLFW_KEY_ESCAPE) || this.window.shouldWindowClose()) {
            this.window.closeWindow();
        }
        stage.update(window, interval);
    }
    
    private void render() {
        stage.render(window);
        window.update();
    }
    
    private void sync() throws InterruptedException {
        float loopSlot = 1f / TARGET_FPS;
        double endTime = timer.getLastLoopTime() + loopSlot;
        while (timer.getTime() < endTime) {
            Thread.sleep(1);
        }
    }
    
    private void cleanup() {
        if (stage != null)
            stage.cleanup();
        
        window.destroy();
    }
}
