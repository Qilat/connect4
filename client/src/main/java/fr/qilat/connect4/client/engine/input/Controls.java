package fr.qilat.connect4.client.engine.input;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_A;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_D;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_DOWN;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_LEFT_SHIFT;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_S;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_UP;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_W;

public class Controls {
    
    public static final int WALK_DOWN = GLFW_KEY_S;
    public static final int WALK_UP = GLFW_KEY_W;
    public static final int WALK_RIGHT = GLFW_KEY_D;
    public static final int WALK_LEFT = GLFW_KEY_A;
    public static final int RUN = GLFW_KEY_LEFT_SHIFT;
    
    
    public static final int GROW_UP = GLFW_KEY_UP;
    public static final int GROW_DOWN = GLFW_KEY_DOWN;
    
}
