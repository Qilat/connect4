package fr.qilat.connect4.client.engine.graphic;

import lombok.Getter;
import org.joml.Quaternionf;
import org.joml.Vector3f;

public class Camera {
    
    public static final float CAMERA_MIN_Z = 10f;
    public static final float CAMERA_MAX_Z = 35f;
    
    @Getter
    private final Vector3f position;
    
    @Getter
    private final Quaternionf rotation;
    
    public Camera(Vector3f position) {
        this.position = position;
        this.rotation = new Quaternionf();
    }
    
    public Camera() {
        this(new Vector3f(0.0f, 0.0f, 0.0f));
    }
    
    public void setPosition(float x, float y, float z) {
        position.x = x;
        position.y = y;
        position.z = z;
    }
    
    public void movePosition(float offsetX, float offsetY, float offsetZ) {
        position.x += offsetX;
        position.y += offsetY;
        position.z += offsetZ;
    }
    
    public void clampPosition(float x, float y, float z) {
        position.x = x;
        position.y = y;
        
        position.z = (z > CAMERA_MAX_Z ? CAMERA_MAX_Z : (Math.max(z, CAMERA_MIN_Z)));
        
    }
}
