package fr.qilat.connect4.client.netty;

import fr.qilat.connect4.server.netty.listeners.ListenersHandler;
import fr.qilat.connect4.server.netty.packets.Packet;
import fr.qilat.connect4.server.netty.packets.PacketDecoder;
import fr.qilat.connect4.server.netty.packets.PacketEncoder;
import fr.qilat.connect4.utils.Logger;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import lombok.Getter;
import lombok.Setter;

public class NettyClient implements Runnable {
    
    private final String host;
    private final int port;
    @Getter
    private final ListenersHandler listenersHandler;
    private PacketClientHandler packetClientHandler;
    private ChannelFuture channelFuture;
    private EventLoopGroup workerGroup;
    
    @Getter
    @Setter
    private boolean connected = false;
    
    @Getter
    @Setter
    private Thread restartThread;
    
    public NettyClient(String host, int port, ListenersHandler listenersHandler) {
        this.host = host;
        this.port = port;
        this.listenersHandler = listenersHandler;
    }
    
    public ChannelFuture connect() throws InterruptedException {
        this.workerGroup = new NioEventLoopGroup();
        
        Bootstrap b = new Bootstrap()
                .group(workerGroup)
                .channel(NioSocketChannel.class)
                .option(ChannelOption.SO_KEEPALIVE, true)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    public void initChannel(SocketChannel ch) throws Exception {
                        ch.pipeline().addLast(
                                new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4),
                                new LengthFieldPrepender(4),
                                new PacketDecoder(),
                                new PacketEncoder(),
                                packetClientHandler = new PacketClientHandler(NettyClient.this));
                    }
                });
        
        return (this.channelFuture = b.connect(host, port)); //.sync());
    }
    
    public void run() {
        try {
            this.connect();
        } catch (InterruptedException e) {
            //e.printStackTrace();
        }
    }
    
    public void stop() {
        if (this.channelFuture != null)
            this.channelFuture.channel().close();
        if (this.workerGroup != null)
            this.workerGroup.shutdownGracefully();
    }
    
    public void sendPacket(Packet packet) {
        this.channelFuture.channel().writeAndFlush(packet);
    }
    
    public void initiateRestartThread() {
        this.restartThread = new Thread(() -> {
            Logger.info("Trying to reconnect to Netty ");
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            NettyClient.this.stop();
            NettyClient.this.run();
        }, "Thread-restartNetty");
        
        this.restartThread.start();
        
    }
}
