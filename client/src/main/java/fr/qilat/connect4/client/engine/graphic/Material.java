package fr.qilat.connect4.client.engine.graphic;

import lombok.Getter;
import lombok.Setter;
import org.joml.Vector4f;

import java.util.ArrayList;
import java.util.List;

public class Material {
    
    private static final Vector4f DEFAULT_COLOUR = new Vector4f(1.0f, 1.0f, 1.0f, 1.0f);
    
    @Getter
    private final List<Texture> textures;
    
    @Getter
    @Setter
    private Vector4f ambientColour = new Vector4f(DEFAULT_COLOUR);
    
    public Material() {
        this.textures = new ArrayList<>();
    }
    
    public Material(Texture texture) {
        this();
        this.textures.add(texture);
    }
    
    public boolean isTextured() {
        return !this.textures.isEmpty() && this.textures.get(0) != null;
    }
    
}
