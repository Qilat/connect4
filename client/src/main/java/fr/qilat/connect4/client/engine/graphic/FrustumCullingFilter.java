package fr.qilat.connect4.client.engine.graphic;

import fr.qilat.connect4.client.engine.item.GameItem;
import org.joml.FrustumIntersection;
import org.joml.Matrix4f;
import org.joml.Vector3f;

import java.util.List;

public class FrustumCullingFilter {
    
    private static final int NUM_PLANES = 6;
    private final Matrix4f prjViewMatrix;
    private final FrustumIntersection frustumIntersection;
    
    public FrustumCullingFilter() {
        prjViewMatrix = new Matrix4f();
        frustumIntersection = new FrustumIntersection();
    }
    
    public void updateFrustum(Matrix4f projMatrix, Matrix4f viewMatrix) {
        prjViewMatrix.set(projMatrix);
        prjViewMatrix.mul(viewMatrix);
        
        frustumIntersection.set(prjViewMatrix);
    }
    
    public boolean insideFrustum(float x0, float y0, float z0, float boundingRadius) {
        return frustumIntersection.testSphere(x0, y0, z0, boundingRadius);
    }
    
    public void filter(List<GameItem> gameItems, float meshBoundingRadius) {
        float boundingRadius;
        Vector3f pos;
        
        for (GameItem gameItem : gameItems) {
            boundingRadius = gameItem.getScale() * meshBoundingRadius;
            pos = gameItem.getPosition();
            gameItem.setInsideFrustum(insideFrustum(pos.x, pos.y, pos.z, boundingRadius));
        }
    }
    
}
