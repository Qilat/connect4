package fr.qilat.connect4.client.engine.item;

import lombok.Getter;
import lombok.Setter;
import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.BodyFixture;
import org.dyn4j.geometry.Vector2;
import org.dyn4j.world.World;

public abstract class PhysicalGameItem extends GameItem {
    
    @Getter
    private final Body physicalBody;
    @Getter
    @Setter
    private boolean collisionless = true;
    
    protected PhysicalGameItem() {
        super();
        this.physicalBody = new Body();
    }
    
    public void init(World<Body> world) {
        super.init();
        if (!this.isCollisionless()) {
            this.getPhysicalBody().translate(this.getPosition().x, this.getPosition().y);
            BodyFixture fixture = this.createFixture(this.getScale());
            this.getPhysicalBody().addFixture(this.createFixture(this.getScale()));
            world.addBody(this.getPhysicalBody());
        }
    }
    
    public void update(World<Body> world, float interval) {
        super.update(interval);
        
        if (this.isScaleChanged()) {
            this.setScaleChanged(false);
            this.physicalBody.removeAllFixtures();
            if (!this.isCollisionless()) {
                BodyFixture fixture = this.createFixture(this.getScale());
                this.getPhysicalBody().addFixture(this.createFixture(this.getScale()));
            }
        }
        if (!this.isCollisionless()) {
            Vector2 physicPosition = this.physicalBody.getWorldCenter();
            this.setPosition((float) physicPosition.x, (float) physicPosition.y, 0.0f);
        }
    }
    
    public void input(World<Body> world) {
        super.input();
    }
    
    /**
     * Will be call in init method before calling children init
     *
     * @param scale scale of humanoid
     * @return BodyFixture of humanoid
     */
    public abstract BodyFixture createFixture(float scale);
    
}
