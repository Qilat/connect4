package fr.qilat.connect4.client.engine.exception;

public class GLFWException extends Exception {
    public GLFWException(String reason) {
        super(reason);
    }
}
