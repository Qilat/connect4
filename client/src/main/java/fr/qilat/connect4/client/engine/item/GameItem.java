package fr.qilat.connect4.client.engine.item;

import fr.qilat.connect4.client.engine.graphic.Mesh;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.joml.Quaternionf;
import org.joml.Vector3f;

/**
 * Created by Theophile on 2018-12-09 for projecty.
 */
public abstract class GameItem {
    
    @Getter
    private final Vector3f position;
    @Getter
    private final Quaternionf rotation;
    @Getter
    @Setter
    private Mesh[] meshes;
    @Getter
    private float scale;
    @Setter(AccessLevel.PROTECTED)
    @Getter
    private boolean scaleChanged = false;
    @Getter
    @Setter
    private boolean visible;
    @Getter
    @Setter
    private boolean insideFrustum;
    
    protected GameItem() {
        this.position = new Vector3f(0, 0, 0);
        this.rotation = new Quaternionf(0, 0, 0, 1.0f);
        this.scale = 1;
        this.visible = true;
        this.meshes = new Mesh[]{};
    }
    
    public void init() {
    }
    
    public void input() {
    }
    
    public void update(float interval) {
    
    }
    
    protected void setRotation(float x, float y, float z) {
        this.rotation.x = x;
        this.rotation.y = y;
        this.rotation.z = z;
    }
    
    public void setPosition(float x, float y, float z) {
        this.position.x = x;
        this.position.y = y;
        this.position.z = z;
    }
    
    public void setPosition3f(Vector3f vector3f) {
        this.position.x = vector3f.x;
        this.position.y = vector3f.y;
        this.position.z = vector3f.z;
    }
    
    public void setScale(float scale) {
        this.scale = scale;
        this.scaleChanged = true;
    }
    
    public Vector3f getRenderPosition() {
        return this.position;
    }
    
    public Mesh getMesh() {
        return this.meshes[0];
    }
    
    public void setMesh(Mesh mesh) {
        this.meshes = new Mesh[]{mesh};
    }
    
}
