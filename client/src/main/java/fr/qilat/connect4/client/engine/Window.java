package fr.qilat.connect4.client.engine;

import fr.qilat.connect4.client.engine.exception.GLFWException;
import fr.qilat.connect4.client.engine.input.Input;
import lombok.Getter;
import lombok.Setter;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.system.MemoryStack;

import java.nio.IntBuffer;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.GLFW_CONTEXT_VERSION_MAJOR;
import static org.lwjgl.glfw.GLFW.GLFW_CONTEXT_VERSION_MINOR;
import static org.lwjgl.glfw.GLFW.GLFW_DECORATED;
import static org.lwjgl.glfw.GLFW.GLFW_FALSE;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_CORE_PROFILE;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_FORWARD_COMPAT;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_PROFILE;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDefaultWindowHints;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;
import static org.lwjgl.glfw.GLFW.glfwGetWindowSize;
import static org.lwjgl.glfw.GLFW.glfwHideWindow;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwSetFramebufferSizeCallback;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetMouseButtonCallback;
import static org.lwjgl.glfw.GLFW.glfwSetScrollCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowPos;
import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL11.GL_FRONT;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TRUE;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glCullFace;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.NULL;

/**
 * Created by Qilat on 2019-03-27 for goldwyn.
 */
public class Window {
    
    @Getter
    private final String title;
    @Getter
    private final boolean vsync;
    @Getter
    private final boolean resizable;
    @Getter
    private long glfwId = -1L;
    @Getter
    private int width;
    @Getter
    private int height;
    @Getter
    @Setter
    private boolean resized;
    @Getter
    private boolean closed;
    @Getter
    private boolean fullscreen = false;
    
    Window(String title, int width, int height, boolean vsync, boolean resizable) {
        this.title = title;
        
        this.width = width;
        this.height = height;
        
        this.vsync = vsync;
        this.resizable = resizable;
    }
    
    void init() throws GLFWException {
        if (Options.DEBUG_OUTPUT)
            GLFWErrorCallback.createPrint(System.err).set();
        
        if (!glfwInit()) {
            throw new IllegalStateException("Unable to initialize GLFW");
        }
        
        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, resizable ? GL_TRUE : GL_FALSE);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        
        if (width == 0 || height == 0) {
            GLFWVidMode vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
            if (vidMode != null) {
                width = vidMode.width();
                height = vidMode.height();
            }
        }
        
        if (this.fullscreen) {
            glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);
        }
        
        glfwId = glfwCreateWindow(width, height, title, fullscreen ? glfwGetPrimaryMonitor() : NULL, glfwId == -1L ? NULL : glfwId);
        
        if (glfwId == NULL) {
            throw new GLFWException("Failed to create the GLFW window");
        }
        
        glfwSetFramebufferSizeCallback(glfwId, (window, width, height) -> {
            this.width = width;
            this.height = height;
            this.setResized(true);
        });
        
        glfwSetKeyCallback(glfwId, Input.keyboard);
        glfwSetMouseButtonCallback(glfwId, Input.mouse);
        glfwSetScrollCallback(glfwId, Input.scroll);
        
        centerWindowOnScreen();
        
        glfwMakeContextCurrent(glfwId);
        
        if (isVsync()) {
            glfwSwapInterval(1);
        }
        
        glfwShowWindow(glfwId);
        
        GL.createCapabilities();
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        
        // Support for transparencies
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        
        //Remove unvisible triangle from render
        glEnable(GL_CULL_FACE);
        glCullFace(GL_FRONT);
    }
    
    void update() {
        glfwSwapBuffers(glfwId);
        Input.update();
    }
    
    void destroy() {
        glfwFreeCallbacks(glfwId);
        glfwDestroyWindow(glfwId);
    }
    
    public void setFullscreen(boolean fullscreen) throws GLFWException {
        this.fullscreen = fullscreen;
        long oldGlfwInt = this.glfwId;
        this.init();
        glfwSetWindowShouldClose(oldGlfwInt, true);
        glfwFreeCallbacks(oldGlfwInt);
        glfwDestroyWindow(oldGlfwInt);
    }
    
    public void setVisible(boolean visible) {
        if (visible)
            glfwShowWindow(glfwId);
        else
            glfwHideWindow(glfwId);
    }
    
    public void setClearColor(float r, float g, float b, float alpha) {
        glClearColor(r, g, b, alpha);
    }
    
    private int[] getWindowSize() {
        MemoryStack stack = stackPush();
        IntBuffer pWidth = stack.mallocInt(1);
        IntBuffer pHeight = stack.mallocInt(1);
        
        glfwGetWindowSize(glfwId, pWidth, pHeight);
        return new int[]{pWidth.get(0), pHeight.get(0)};
    }
    
    private void centerWindowOnScreen() {
        int[] dim = getWindowSize();
        GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        if (vidmode != null)
            glfwSetWindowPos(
                    this.glfwId,
                    (vidmode.width() - dim[0]) / 2,
                    (vidmode.height() - dim[1]) / 2
            );
    }
    
    public boolean shouldWindowClose() {
        return glfwWindowShouldClose(this.glfwId);
    }
    
    public void closeWindow() {
        glfwSetWindowShouldClose(glfwId, true);
        this.closed = true;
    }
    
}
