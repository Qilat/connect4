package fr.qilat.connect4.client.engine.graphic;

import fr.qilat.connect4.client.engine.item.GameItem;
import lombok.Getter;
import org.joml.Matrix4f;
import org.joml.Vector3f;

/**
 * Created by Theophile on 2018-12-09 for projecty.
 */
public class Transformation {
    
    @Getter
    private final Matrix4f viewMatrix;
    @Getter
    private final Matrix4f projectionMatrix;
    private final Matrix4f modelMatrix;
    private final Matrix4f orthoModelMatrix;
    private final Matrix4f ortho2DProjectionMatrix;
    
    public Transformation() {
        viewMatrix = new Matrix4f();
        projectionMatrix = new Matrix4f();
        modelMatrix = new Matrix4f();
        ortho2DProjectionMatrix = new Matrix4f();
        orthoModelMatrix = new Matrix4f();
    }
    
    public void updateProjectionMatrix(float fov, float width, float height, float zNear, float zFar) {
        projectionMatrix.identity();
        projectionMatrix.setPerspective(fov, width / height, zNear, zFar);
    }
    
    public void updateViewMatrix(Camera camera) {
        Vector3f cameraPos = camera.getPosition();
        viewMatrix.identity();
        viewMatrix.translate(-cameraPos.x, -cameraPos.y, -cameraPos.z);
        viewMatrix.rotate(camera.getRotation());
    }
    
    public Matrix4f getModelViewMatrix(GameItem gameItem) {
        modelMatrix.translationRotateScale(
                gameItem.getRenderPosition().x, gameItem.getRenderPosition().y, gameItem.getRenderPosition().z,
                gameItem.getRotation().x, gameItem.getRotation().y, gameItem.getRotation().z, 1.0f,
                gameItem.getScale());
        return new Matrix4f(this.viewMatrix).mul(modelMatrix);
    }
    
    public final void updateOrtho2DProjectionMatrix(float left, float right, float bottom, float top) {
        ortho2DProjectionMatrix.identity();
        ortho2DProjectionMatrix.setOrtho2D(left, right, bottom, top);
    }
    
    public Matrix4f buildModelMatrix(GameItem gameItem) {
        return modelMatrix.translationRotateScale(
                gameItem.getPosition().x, gameItem.getPosition().y, gameItem.getPosition().z,
                gameItem.getRotation().x, gameItem.getRotation().y, gameItem.getRotation().z, 1.0f,
                gameItem.getScale());
    }
    
    public Matrix4f getOrtho2DProjectionMatrix() {
        return new Matrix4f(this.ortho2DProjectionMatrix);
    }
}
