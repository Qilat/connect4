package fr.qilat.connect4.client.engine;

import java.nio.charset.StandardCharsets;
import java.util.Locale;

public class Options {
    
    public static final boolean DEBUG_OUTPUT = true;
    public static final String CHARSET = StandardCharsets.ISO_8859_1.name();
    
    public static Locale LOCALE = Locale.FRANCE;
}
