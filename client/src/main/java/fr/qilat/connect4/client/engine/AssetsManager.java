package fr.qilat.connect4.client.engine;

import fr.qilat.connect4.client.engine.graphic.Texture;

import java.util.HashMap;

public class AssetsManager {
    
    private static final AssetsManager instance = new AssetsManager();
    
    private final HashMap<String, Texture> textures;
    
    private AssetsManager() {
        this.textures = new HashMap<>();
    }
    
    public static AssetsManager get() {
        return instance;
    }
    
    public Texture getTexture(String path) {
        try {
            Texture texture = textures.get(path);
            if (texture != null)
                return texture;
            texture = new Texture(path);
            textures.put(path, texture);
            return texture;
        } catch (Exception e) {
            if (Options.DEBUG_OUTPUT)
                e.printStackTrace();
        }
        
        return null;
    }
    
    public void deleteTexture(String path) {
        Texture texture = textures.remove(path);
        if (texture != null)
            texture.cleanup();
    }
    
    public void cleanUp() {
        for (java.util.Map.Entry<String, Texture> entry : textures.entrySet()) {
            entry.getValue().cleanup();
        }
    }
}
