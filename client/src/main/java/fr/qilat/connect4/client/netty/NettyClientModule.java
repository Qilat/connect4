package fr.qilat.connect4.client.netty;

import fr.qilat.connect4.client.netty.listener.UUIDPacketListener;
import fr.qilat.connect4.server.netty.listeners.ListenersHandler;
import fr.qilat.connect4.server.netty.packets.Packet;
import fr.qilat.connect4.utils.IModule;
import fr.qilat.connect4.utils.Logger;
import lombok.Getter;

import java.util.UUID;

public class NettyClientModule implements IModule {
    
    private final NettyClient nettyClient;
    @Getter
    private final ListenersHandler listenersHandler;
    @Getter
    private UUID personalUuid;
    
    public NettyClientModule(String host, int port) {
        this.listenersHandler = new ListenersHandler();
        this.nettyClient = new NettyClient(host, port, this.listenersHandler);
    }
    
    @Override
    public void start() {
        Logger.info("Starting Netty module.");
        this.listenersHandler.registerListener(new UUIDPacketListener(this));
        this.nettyClient.run();
    }
    
    @Override
    public void update() {
        this.listenersHandler.processReceivedPackets();
    }
    
    @Override
    public void stop() {
        Logger.info("Stopping Netty module.");
        this.nettyClient.stop();
    }
    
    public void setPersonalUuid(UUID personalUuid) {
        this.personalUuid = personalUuid;
    }
    
    public void sendPacket(Packet packet) {
        this.nettyClient.sendPacket(packet);
    }
}
