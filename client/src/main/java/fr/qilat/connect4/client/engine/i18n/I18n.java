package fr.qilat.connect4.client.engine.i18n;

import fr.qilat.connect4.client.engine.Options;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class I18n {
    
    private static ClassLoader loader;
    private static Map<String, ResourceBundle> bundles;
    
    public static void init() {
        if (bundles == null)
            bundles = new HashMap<>();
        
        File file = new File("i18n");
        URL[] urls = new URL[0];
        try {
            urls = new URL[]{file.toURI().toURL()};
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        loader = new URLClassLoader(urls);
    }
    
    public static void preload(String[] baseNames) {
        for (String baseName : baseNames) {
            bundles.put(baseName, ResourceBundle.getBundle(baseName, Options.LOCALE, loader));
        }
    }
    
    public static void preload(String baseName) {
        I18n.preload(new String[]{baseName});
    }
    
    public static String getString(String baseName, String key) {
        return bundles.get(baseName).getString(key);
    }
}
