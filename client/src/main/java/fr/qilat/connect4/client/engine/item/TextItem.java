package fr.qilat.connect4.client.engine.item;

import fr.qilat.connect4.client.engine.graphic.FontTexture;
import fr.qilat.connect4.client.engine.graphic.Material;
import fr.qilat.connect4.client.engine.graphic.Mesh;
import fr.qilat.connect4.client.engine.utils.Utils;

import java.util.ArrayList;
import java.util.List;


public class TextItem extends GameItem {
    
    private static final float ZPOS = 0.0f;
    
    private static final int VERTICES_PER_QUAD = 4;
    
    private final FontTexture fontTexture;
    
    private String text;
    
    public TextItem(String text, FontTexture fontTexture) {
        super();
        this.text = text;
        this.fontTexture = fontTexture;
        this.setMesh(buildMesh());
    }
    
    private Mesh buildMesh() {
        List<Float> positions = new ArrayList<>();
        List<Float> textCoords = new ArrayList<>();
        List<Integer> indices = new ArrayList<>();
        char[] characters = text.toCharArray();
        int numChars = characters.length;
        
        float startx = 0;
        for (int i = 0; i < numChars; i++) {
            FontTexture.CharInfo charInfo = fontTexture.getCharInfo(characters[i]);
            // Left Bottom vertex: i
            positions.add(startx); // x
            positions.add(0.0f); //y
            positions.add(ZPOS); //z
            textCoords.add((float) charInfo.getStartX() / (float) fontTexture.getWidth());
            textCoords.add(0.0f);
            
            // Right Bottom vertex : i + 1
            positions.add(startx + charInfo.getWidth()); // x
            positions.add(0.0f); //y
            positions.add(ZPOS); //z
            textCoords.add((float) (charInfo.getStartX() + charInfo.getWidth()) / (float) fontTexture.getWidth());
            textCoords.add(0.0f);
            
            // Left Top vertex : i + 2
            positions.add(startx); // x
            positions.add((float) fontTexture.getHeight()); //y
            positions.add(ZPOS); //z
            textCoords.add((float) charInfo.getStartX() / (float) fontTexture.getWidth());
            textCoords.add(1.0f);
            
            // Right Top vertex : i + 3
            positions.add(startx + charInfo.getWidth()); // x
            positions.add((float) fontTexture.getHeight()); //y
            positions.add(ZPOS); //z
            textCoords.add((float) (charInfo.getStartX() + charInfo.getWidth()) / (float) fontTexture.getWidth());
            textCoords.add(1.0f);
            
            indices.add(i * VERTICES_PER_QUAD);
            indices.add(i * VERTICES_PER_QUAD + 1);
            indices.add(i * VERTICES_PER_QUAD + 2);
            
            indices.add(i * VERTICES_PER_QUAD + 3);
            indices.add(i * VERTICES_PER_QUAD + 2);
            indices.add(i * VERTICES_PER_QUAD + 1);
            
            startx += charInfo.getWidth();
        }
        
        float[] posArr = Utils.listToArray(positions);
        float[] textCoordsArr = Utils.listToArray(textCoords);
        int[] indicesArr = indices.stream().mapToInt(i -> i).toArray();
        Mesh mesh = new Mesh(posArr, textCoordsArr, indicesArr);
        mesh.setMaterial(new Material(fontTexture.getTexture()));
        return mesh;
    }
    
    public String getText() {
        return text;
    }
    
    public void setText(String text) {
        this.text = text;
        this.getMesh().deleteBuffers();
        this.setMesh(buildMesh());
    }
}
