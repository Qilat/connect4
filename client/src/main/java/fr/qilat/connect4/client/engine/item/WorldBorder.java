package fr.qilat.connect4.client.engine.item;

import org.dyn4j.collision.CategoryFilter;
import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.BodyFixture;
import org.dyn4j.geometry.Geometry;
import org.dyn4j.geometry.MassType;
import org.dyn4j.geometry.Vector2;
import org.dyn4j.world.World;

public class WorldBorder extends PhysicalGameItem {
    
    private final boolean vertical;
    private final float length;
    
    public WorldBorder(boolean vertical, float length, Vector2 position) {
        super();
        this.setCollisionless(false);
        this.setVisible(false);
        this.vertical = vertical;
        this.length = length;
        this.getPosition().x = (float) position.x;
        this.getPosition().y = (float) position.y;
    }
    
    @Override
    public void init(World<Body> world) {
        super.init(world);
        this.getPhysicalBody().setMass(MassType.INFINITE);
    }
    
    @Override
    public void input(World<Body> world) {
        // empty method
    }
    
    @Override
    public BodyFixture createFixture(float scale) {
        double width = scale * (this.vertical ? 1.0 : this.length);
        double height = scale * (this.vertical ? this.length : 1.0);
        BodyFixture bf = new BodyFixture(Geometry.createRectangle(width, height));
        bf.setFilter(new CategoryFilter(1, Long.MAX_VALUE));
        return bf;
    }
}
