package fr.qilat.connect4.client;

import fr.qilat.connect4.client.netty.NettyClientModule;
import fr.qilat.connect4.server.console.ConsoleModule;
import fr.qilat.connect4.server.console.commands.StopCommand;
import fr.qilat.connect4.utils.IModule;
import fr.qilat.connect4.utils.Logger;
import lombok.Getter;

public class Connect4Client implements IModule {
    
    private static Connect4Client CONNECT_4_CLIENT;
    private final NettyClientModule nettyClientModule;
    @Getter
    private final ConsoleModule consoleModule;
    private boolean shouldStop = false;
    
    public Connect4Client(String serverAdress, int serverPort) {
        CONNECT_4_CLIENT = this;
        this.nettyClientModule = new NettyClientModule(serverAdress, serverPort);
        this.consoleModule = new ConsoleModule();
    }
    
    public static Connect4Client get() {
        return CONNECT_4_CLIENT;
    }
    
    @Override
    public void start() {
        Logger.info("Starting all modules.");
        this.nettyClientModule.start();
        
        this.consoleModule.getRegister().registerCommand("stop", new StopCommand(this, this.consoleModule));
        this.consoleModule.start();
        
        while (!shouldStop) {
            this.consoleModule.update();
            this.nettyClientModule.update();
        }
    }
    
    @Override
    public void update() {
    
    }
    
    @Override
    public void stop() {
        Logger.info("Stopping all modules.");
        
        this.shouldStop = true;
        this.nettyClientModule.stop();
        
        if (this.consoleModule.isRunning())
            System.exit(0);
    }
    
}
