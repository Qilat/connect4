package fr.qilat.connect4.client.engine;

import fr.qilat.connect4.client.engine.item.GameItem;

import java.util.List;

/**
 * Created by Theophile on 2018-12-11 for ProjectY.
 */
public interface IGui {
    
    List<GameItem> getGuiElements();
    
    default void cleanup() {
        for (GameItem guiElement : getGuiElements()) {
            guiElement.getMesh().cleanUp();
        }
    }
    
    void update(float interval);
    
    void updateSize(Window window);
}
