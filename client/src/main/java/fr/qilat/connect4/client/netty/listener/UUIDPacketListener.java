package fr.qilat.connect4.client.netty.listener;

import fr.qilat.connect4.client.netty.NettyClientModule;
import fr.qilat.connect4.server.netty.listeners.PacketListener;
import fr.qilat.connect4.server.netty.listeners.PacketListenersContainer;
import fr.qilat.connect4.server.netty.packets.UuidPacket;
import fr.qilat.connect4.utils.Logger;

public class UUIDPacketListener implements PacketListenersContainer {
    
    private final NettyClientModule module;
    
    public UUIDPacketListener(NettyClientModule module) {
        this.module = module;
    }
    
    @PacketListener(UuidPacket.class)
    public void attribUUID(UuidPacket packet) {
        this.module.setPersonalUuid(packet.getUuid());
        Logger.info("UUID set as " + packet.getUuid());
    }
}
