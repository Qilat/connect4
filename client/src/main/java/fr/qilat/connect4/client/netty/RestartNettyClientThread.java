package fr.qilat.connect4.client.netty;

public class RestartNettyClientThread extends Thread {
    
    private final NettyClient nettyClient;
    
    public RestartNettyClientThread(NettyClient nettyClient) {
        this.nettyClient = nettyClient;
    }
    
    @Override
    public void run() {
        
        try {
            this.nettyClient.connect();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        
    }
}
